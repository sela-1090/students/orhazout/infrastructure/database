# PostgreSQL installation using Helm


## Prerequisites
1. kubernetes cluster
2. helm chart

### Add a Helm repository named "bitnami"
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### updates the local cache of available Helm charts from all added repositories
```
helm repo update
```

### install a Helm release named "my-postgresql"
```
helm install my-postgresql bitnami/postgresql --version 12.7.1 --namespace database
```

### To get the "postgres" user's password, run the following command (Change the values in [] brackets to match your environment):
```
kubectl get secret --namespace database my-postgresql -o jsonpath="{.data.postgres-password}"
```

### Next, connect to the server and run the psql command
```
kubectl exec -it my-postgresql --namespace database -- /opt/bitnami/scripts/postgresql/entrypoint.sh /bin/bash
```
```
psql 
```


### To use custom values in the deployment of PostgreSQL:

Create a values.yaml file
insert the desired values into the file
run the following command:
```
helm install postgresql -f values.yaml bitnami/postgresql --namespace database
```
